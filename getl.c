#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

bool d1 = false;
bool d2 = false;

int getl(char s[], int mxl)
{
    int ind = 0;
    int c;
    while (ind < mxl-1 && (c = getchar()) != EOF)
    {
        *(s+ind) = c;
        ind++;
        if (c == '\n')
        {
            break;
        }
    }

    *(s+ind) = '\0';
    int len = strlen(s);
    if (d2) printf("String is '%s'\nlen = %i\n", s, len);

    return len;
}


int main()
{
    char s[10];
    while (getl(s, 10) == 10)
    {
        printf("%s", s);
    }
    printf("%s", s);
}
